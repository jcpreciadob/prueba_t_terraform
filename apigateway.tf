resource "aws_api_gateway_rest_api" "my_rest_api" {
  name        = "my-rest-api"
  description = "My REST API"
}

resource "aws_api_gateway_resource" "devops_resource" {
  rest_api_id = aws_api_gateway_rest_api.my_rest_api.id
  parent_id   = aws_api_gateway_rest_api.my_rest_api.root_resource_id
  path_part   = "DevOps"
}

resource "aws_api_gateway_method" "post_method" {
  rest_api_id   = aws_api_gateway_rest_api.my_rest_api.id
  resource_id   = aws_api_gateway_resource.devops_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "my_integration" {
  rest_api_id             = aws_api_gateway_rest_api.my_rest_api.id
  resource_id             = aws_api_gateway_resource.devops_resource.id
  http_method             = aws_api_gateway_method.post_method.http_method
  integration_http_method = "POST"
  type                    = "HTTP_PROXY"
  uri                     = "http://${aws_lb.app_load_balancer.dns_name}/DevOps"
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.my_rest_api.id
  resource_id = aws_api_gateway_resource.devops_resource.id
  http_method = aws_api_gateway_method.post_method.http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.my_rest_api.id
  resource_id = aws_api_gateway_resource.devops_resource.id
  http_method = aws_api_gateway_method.post_method.http_method
  status_code = aws_api_gateway_method_response.response_200.status_code
}

resource "aws_api_gateway_authorizer" "api_key_authorizer" {
  rest_api_id                     = aws_api_gateway_rest_api.my_rest_api.id
  name                            = "api-key-authorizer"
  type                            = "API_KEY"
  identity_source                 = "method.request.header.x-api-key"
  authorizer_result_ttl_in_seconds = 300
}

resource "aws_api_gateway_authorizer" "jwt_authorizer" {
  rest_api_id                     = aws_api_gateway_rest_api.my_rest_api.id
  name                            = "jwt-authorizer"
  type                            = "JWT"
  identity_source                 = "method.request.header.Authorization"
  authorizer_result_ttl_in_seconds = 300
  jwt_configuration {
    audience = ["my-audience"]
    issuer   = "my-issuer"
  }
}

resource "aws_api_gateway_usage_plan" "my_usage_plan" {
  name        = "my-usage-plan"
  description = "My Usage Plan"

  api_stages {
    api_id   = aws_api_gateway_rest_api.my_rest_api.id
    stage    = "dev"
    throttle {
      burst_limit = 5000
      rate_limit  = 10000
    }
  }

  quota_settings {
    limit   = 100000
    offset  = 2
    period  = "MONTH"
  }
}

resource "aws_api_gateway_usage_plan_key" "api_key_usage_plan_key" {
  key_id        = aws_api_key.my_api_key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.my_usage_plan.id
}

resource "aws_api_gateway_usage_plan_key" "jwt_usage_plan_key" {
  key_id        = aws_iam_user.my_iam_user.encrypted_secret
  key_type      = "JWT"
  usage_plan_id = aws_api_gateway_usage_plan.my_usage_plan.id
}
