# Crea el clúster de Fargate
resource "aws_ecs_cluster" "cluster_appTecnica" {
  name = "appTecnica"
}

# Crea el repositorio de ECR
resource "aws_ecr_repository" "ecr_Repo_App" {
  name = "app-repo-tecnica"
}

# Crea la definición de la tarea de Fargate
resource "aws_ecs_task_definition" "app_task_definition_Tecnica" {
  family                   = "app-task-definition-Tecnica"
  cpu                      = "256"
  memory                   = "512"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.ecsTaskExecutionRoleTecnica.arn
  container_definitions    = <<EOF
[
  {
    "name": "APP-Tecnica",
    "image": "${aws_ecr_repository.ecr_Repo_App.repository_url}:latest",
    "portMappings": [
      {
        "containerPort": 80,
        "protocol": "tcp"
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "app-log-group",
        "awslogs-region": "us-east-1",
        "awslogs-stream-prefix": "app-Tecnica-Service"
      }
    }
  }
]
EOF
}

# Crea el servicio de Fargate
resource "aws_ecs_service" "appTecnicaService" {
  name             = "appTecnicaService-Name"
  cluster          = aws_ecs_cluster.cluster_appTecnica.id
  task_definition  = aws_ecs_task_definition.app_task_definition_Tecnica.arn
  desired_count    = 2
  launch_type      = "FARGATE"
  platform_version = "1.3.0"
  

  network_configuration {
    subnets = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
    security_groups = [aws_security_group.sgTecnica.id]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.tg_AppTecnica.arn
    container_name   = "APP-Tecnica"
    container_port   = 80
  }
}
