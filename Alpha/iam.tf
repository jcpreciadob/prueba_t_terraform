resource "aws_iam_role" "ecsTaskExecutionRoleTecnica" {
  name = "ecsTaskExecutionRoleTecnica"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          "Service" : "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "CloudWatch_Policy" {
  role       = aws_iam_role.ecsTaskExecutionRoleTecnica.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}

resource "aws_iam_role_policy_attachment" "ECSTaskExecutionRoleTPolicy" {
  role       = aws_iam_role.ecsTaskExecutionRoleTecnica.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ECR_Policy" {
  role       = aws_iam_role.ecsTaskExecutionRoleTecnica.name
  policy_arn = "arn:aws:iam::aws:policy/EC2InstanceProfileForImageBuilderECRContainerBuilds"
}

resource "aws_iam_role_policy_attachment" "RunnerECR_Policy" {
  role       = aws_iam_role.ecsTaskExecutionRoleTecnica.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSAppRunnerServicePolicyForECRAccess"
}
